# HeyaCare management app
## Features/packages included

- [TypeScript](https://www.typescriptlang.org/) - Type checker.
- [Ant Design](https://ant.design/components/overview/) - Build layout & components.
- [axios](https://github.com/axios/axios) - Promise based HTTP client.
- [react-i18next](https://github.com/i18next/react-i18next) - Multi-language support (see [locales](#locales) section for usage).
- [ESLint](https://eslint.org/) and [Prettier](https://prettier.io/) - Coding convention.

> Note: You can install more packages which are familiar to you (eg: redux, moment, lodash...) and implement features as you want.

## Installation

Use the package manager [npm](https://www.npmjs.com/) or [yarn](https://www.npmjs.com/package/yarn) to install Teko Admin Boilerplate.

```
yarn install
```

Run the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

```
yarn start
```

## Configuration

### Environment variables

- To customize environment variables, override values from `public/env.js` to `public/configs/env-local.js` (create this file if it doesn't exist). Don't forget to uncomment the line `<script src="/configs/env-local.js"></script>` in `public/index.html`.

### App configuration

- Change the app logo by replacing `src/assets/logo.png` with your logo.
- Change the meta data, favicon... according to your app in `public/index.html`.
- The default theme of application is customized using LESS variables in `src/config-overrides.js`. You can change these variables to match your own theme.
- Setup CI/CD pipeline.

### Testing

- The coverage threshold is configured in `package.json`, default coverage threshold for coding branches, functions, lines and statements is 50%, 50%, 70% and 50% respectively. You can change these values to match your own rules.
- The boilerplate uses [react-testing-library](https://testing-library.com/docs/react-testing-library/intro) as the testing library. Because of the complexity of AntD components, we add a `src/mockAntd.tsx` file which is used to mock AntD components for easier testing. These mock components will be used as global setup for testing.

## Folder structure

We will go inside the `src` directory of the boilerplate because [webpack](https://webpack.js.org/) will only process files inside `src` for faster rebuilds. You may create subdirectories inside `src` as suggested below.

> Note: This folder structure is highly recommended, but it is not the best structure for a React app. You can re-structure to whatever you feel that is the best for you.

### assets

Define all assets for application (eg: images, external CSS, JS, font files...).

### components

You can write components for application inside this folder. Each component should be self-contained as a module (includes specific view, child components, style, type, helper, hook, interface, test, storybook...).

Example:

```
components
├── shared
│   └── ContentBlock
│       ├── ContentBlock.tsx
│       ├── ContentBlock.test.tsx
│       ├── ContentBlock.scss
│       └── index.tsx
└── Product
    └── ProductFilter
        ├── ProductFilter.tsx
        ├── ProductFilter.test.tsx
        ├── ProductFilter.scss
        └── index.tsx
```

### constants

Define all shared constants used in application. There should be an `index.ts` file for faster import.

Example:

```typescript
// src/constants/common.ts
// This file declares common constants used in app.

export default {
  ALL: 'all',
  TABLET_WIDTH: 768,
};
```

```typescript
// src/constants/index.ts
// Export constants from all modules

export { default as commonConstants } from './common';
```

### containers

We may call a component which is a page/screen as a container. Each container will link to a route in the [router](https://reactrouter.com/web/guides/quick-start) system.

> Note: You can rename this folder to whatever you want: pages, screens...

Example:

```typescript
// src/containers/Home.tsx
// This is a container for Home page (link to '/' route).

import React from 'react';
import { t } from 'helpers/i18n';
import AppContainer from 'containers/AppLayout/AppContainer';

const Home: React.FC = () => {
  return <AppContainer title={t('WelcomeText')} />;
};

export default Home;
```

### contexts

Define all shared contexts used in application.

Example:

```typescript
// src/contexts/index.tsx
// Write app contexts in this file (eg: a context which stores global data for the whole app).

import { createContext } from 'react';
import { IUserDetailsInfo } from 'teko-oauth2';
import { currentUser } from 'services/mocks/user';

interface StoreContextType {
  currentUser: IUserDetailsInfo;
}

export const StoreContext = createContext<StoreContextType>({
  currentUser,
});
```

### helpers

You can write shared helpers/utils here. There should be an `index.ts` file for faster import.

Example:

```typescript
// src/helpers/common.ts
// This file has a helper which gets the current window dimensions.

const getWindowDimensions = () => {
  return {
    width: window.innerWidth,
    height: window.innerHeight,
  };
};

export default {
  getWindowDimensions,
};
```

```typescript
// src/helpers/index.ts
// Export helpers from all modules.

export { default as commonHelpers } from './common';
```

### hooks

Write custom shared hooks (handle non-visual logic) for application. There should be an `index.ts` file for faster import.

Example:

```typescript
// src/hooks/common.ts
// This file has a hook which triggers the window resize event and return the current window dimension.

import { useState, useEffect } from 'react';
import commonHelpers from 'helpers/common';

const { getWindowDimensions } = commonHelpers;

const useWindowDimensions = () => {
  const [dimensions, setDimensions] = useState(getWindowDimensions);

  useEffect(() => {
    const handleResize = () => setDimensions(getWindowDimensions());
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return dimensions;
};

export default {
  useWindowDimensions,
};
```

```typescript
// src/hooks/index.ts
// Export hooks from all modules.

export { default as commonHooks } from './common';
```

### interfaces

Define all shared types/interfaces using [TypeScript](https://www.typescriptlang.org/). You should create an `index.ts` file to export all interfaces inside this folder.

Example:

```typescript
// src/interfaces/product.ts
// This file defines all interfaces for the product module.

export interface IProduct {
  id: number;
  name: string;
  brand: string;
  status: string;
}
```

```typescript
// src/interfaces/index.ts
// Export interfaces from all modules.

export * from './product';
```

### locales

Define texts for multi-language using [react-i18next](https://github.com/i18next/react-i18next).

Example:

```typescript
// src/locales/vi/translation.json

{
  "Home": "Trang chủ",
  "WelcomeText": "Chào mừng bạn đến với HeyaCare"
}
```

```typescript
// src/locales/en/translation.json

{
  "Home": "Home",
  "WelcomeText": "Welcome to HeyaCare"
}
```

```typescript
// use translation in any files

import i18n from 'i18n';

console.log(i18n.t('WelcomeText'));
```

### routes

You can config menus/routes for the application here (see example below - integrated with IAM permissions).

> Note: If the router system is very large, you will want to split routes into smaller files for better management.

Example:

```typescript
// src/routes/index.ts
// This example uses catalog service as authorization service. You can change permissions to your IAM service's permissions for testing.

import { lazy } from 'react';
import { HomeOutlined, AppstoreOutlined } from '@ant-design/icons';
import {
  appConstants,
  resourceConstants,
  actionConstants,
} from 'constants/index';
import { t } from 'helpers/i18n';

// App pages
const Home = lazy(() => import('containers/Home'));
const ProductList = lazy(() => import('containers/Product/ProductList'));
const ProductDetail = lazy(() => import('containers/Product/ProductDetail'));
const ProductCreate = lazy(() => import('containers/Product/ProductCreate'));

/*
 * If route has children, it's a parent menu (not link to any pages)
 */
const routes = [
  // This is a menu/route which has no children (sub-menu)
  {
    exact: true,
    path: '/',
    name: t('Home'),
    component: Home,
    icon: HomeOutlined,
  },
  // This is a parent menu which has children (sub-menu) and requires catalog:product:X permission to display
  // X maybe read/create/update/delete...
  {
    path: '/products',
    name: t('Products'),
    icon: AppstoreOutlined,
    children: ['/products', '/products/create'], // Specifies sub-menus/routes (sub-menu path)
  },
  // This is a sub-menu/route which requires catalog:product:read permission to display/access
  {
    exact: true,
    path: '/products',
    name: t('ProductList'),
    component: ProductList,
  },
  // This is a sub-menu/route which requires catalog:product:create permission to display/access
  {
    exact: true,
    path: '/products/create',
    name: t('ProductCreate'),
    component: ProductCreate,
  },
  // This is a route which requires catalog:product:update permission to access
  {
    exact: true,
    path: '/products/:id',
    name: t('ProductDetail'),
    component: ProductDetail,
  },
];

export default routes;
```

### services

Define all shared services which get result (from get/post/put... requests) for components to render. The result can be received from API, mock, local storage... depends on usage (API integration, UI tesing, unit testing...). There should be an `index.ts` file for faster import.

Example:

```typescript
// src/services/apis/auth.ts
// This file has a function which calls the real API from TekoID to get full user info.

const getFullUserInfo = async () => {
  const fullUserInfo = await user.getFullUserInfo();
  return fullUserInfo;
};

export default {
  getFullUserInfo,
};
```

```typescript
// src/services/mocks/auth.ts
// This file has a function which returns a mock data for user info (not call any APIs).
// Mock data will be used for UI testing, unit testing...

const getFullUserInfo = () => {
  return Promise.resolve({
    sub: '1',
    name: 'Ngo Sach Nhat',
    email: 'nhatns.uet@gmail.com',
  });
};

export default {
  getUserInfo,
};
```

```typescript
// src/services/index.ts
// Export services from all modules.

// API services
export { default as userServices } from './apis/user';

// Mock services (used for testing only)
export { default as userMockServices } from './mocks/user';
```

### stories

This is the default storybook folder.

> Note: You can write stories for your components here or inside the component's folder for better management.

Run the storybook with the following command

```
yarn storybook
```

### styles

This folder will define common/shared styles for application:

- **variables.scss**: Define CSS variables according to the Design System of application.
- **customizeAntd.scss**: Override styles for AntD components,
- **common.scss**: Define common (global) styles used for the whole app.

> Note: This folder defines common styles for application only. If you want to define styles for each component, write it inside the component's folder for better management.

## Next steps
