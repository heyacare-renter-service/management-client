import { Card, Form, Divider, Input, Typography, Select, Button } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import Avatar from 'antd/lib/avatar/avatar';
import ContentBlock from 'components/shared/ContentBlock';
import AppContainer from 'containers/AppLayout/AppContainer';
import { StoreContext } from 'contexts';
import React, { useContext, useState } from 'react';
import './Profile.scss';
import { browserHistory } from 'helpers';
const Profile = () => {
  const { currentUser } = useContext(StoreContext);
  const [isEditing, setIsEditing] = useState(false);
  const { Option } = Select;
  const [name, setName] = useState({ first_name: '', last_name: '' });

  const handleFinish = (values: any) => {
    console.log(values);
  };
  return (
    <AppContainer title={'Trang cá nhân'}>
      <ContentBlock className="profile-page">
        <Typography.Title className={'text-center'} level={4}>
          Xem thông tin cá nhân của bạn
        </Typography.Title>

        <Card bordered={false} className="profile-information">
          <Avatar src={currentUser.avatar_url} className="avatar"></Avatar>

          <Typography.Text strong className="fullname">
            {currentUser.fullname}
          </Typography.Text>

          <Typography.Text type="secondary" className="email">
            {currentUser.email}
          </Typography.Text>

          <Typography.Text type="secondary" className="email">
            {currentUser.address}
          </Typography.Text>
        </Card>
        <Divider />
        <Typography.Title className={'text-center'} level={4}>
          Chỉnh sửa thông tin
          <EditOutlined
            onClick={() => {
              setIsEditing(!isEditing);
            }}
          />
        </Typography.Title>
        {isEditing ? (
          <Card bordered={false} className="profile-information">
            <Form className="form-profile" onFinish={handleFinish}>
              <div className="fill-name">
                <Form.Item
                  name="first_name"
                  label="Họ"
                  rules={[{ required: true }]}
                >
                  <Input
                    value={name.first_name}
                    onChange={e =>
                      setName({ ...name, first_name: e.target.value })
                    }
                  />
                </Form.Item>
                <Form.Item
                  name="last_name"
                  label="Tên"
                  rules={[{ required: true }]}
                >
                  <Input
                    value={name.last_name}
                    onChange={e =>
                      setName({ ...name, last_name: e.target.value })
                    }
                  />
                </Form.Item>
              </div>
              {name.first_name && name.last_name && (
                <Form.Item
                  name="fullname"
                  label="Thứ tự tên hiển thị"
                  rules={[{ required: true }]}
                >
                  <Select defaultValue="Tên đầy đủ">
                    <Option value="0">
                      {name.first_name} {name.last_name}
                    </Option>
                    <Option value="1">
                      {name.last_name} {name.first_name}
                    </Option>
                  </Select>
                </Form.Item>
              )}
              <Form.Item
                name="address"
                label="Địa chỉ"
                rules={[{ required: true }]}
              >
                <Input />
              </Form.Item>
              <Button type="primary" htmlType="submit">
                Sửa thông tin
              </Button>
              <Button
                onClick={value => {
                  browserHistory.push('/');
                }}
              >
                Click here
              </Button>
            </Form>
          </Card>
        ) : (
          <></>
        )}
      </ContentBlock>
    </AppContainer>
  );
};

export default Profile;
