import React, { useState } from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import './Login.scss';
import logo from '../../../assets/images/logo.png';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { authServices } from '../../../services';
import { Link, Redirect } from 'react-router-dom';


const Login = (props: any) => {
  const { from } = props.location.state || { from: { pathname: '/' } };

  const [isLogged, setIsLogged] = useState(false);

  if (isLogged || authServices.isLoggedIn()) {
    return <Redirect to={from} />;
  }

  const onFinish = (values: any) => {
    authServices.login(values.username, values.password).then(
      (res: any) => {
        authServices.setAccessToken(res.data.access_token);
        setIsLogged(true);
      },
    );
  };

  return (
    <div className={'login'}>
      <div className="login__body">
        <img src={logo} alt={'heyacare_logo'} />
        <h3> Quản lí và cho thuê phòng trọ thật dễ dàng 😍</h3>
        <Form
          name="normal_login"
          className="login-form login__body__form"
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: 'Hãy nhập email hoặc số điện thoại !' }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Email/Số điện thoại ..."
              className={'input'}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: 'Hãy nhập mật khẩu!' }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Mật khẩu ..."
              className={'input'}
            />
          </Form.Item>
          <Form.Item>
            <Form.Item className="login-form-remember" name="remember" noStyle>
              <Checkbox>Ghi nhớ lần sau</Checkbox>
            </Form.Item>

            <a className="login-form-forgot" href="">
              Quên mật khẩu
            </a>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button mb-base">
              Đăng nhập
            </Button>
            Hoặc <Link to={'/register'}>Đăng ký ngay!</Link>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default Login;