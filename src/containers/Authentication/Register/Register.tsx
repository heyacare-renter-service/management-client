import React, { useState } from 'react';
import { authServices, ownerServices } from '../../../services';
import { Link, Redirect } from 'react-router-dom';
import logo from '../../../assets/images/logo.png';
import { Button, Checkbox, Form, Input, notification, Select, Typography } from 'antd';
import './Register.scss';

const Option = Select.Option;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 24,
      offset: 0,
    },
  },
};

const Register = (props: any) => {
  const { from } = props.location.state || { from: { pathname: '/' } };

  const [formVisible, setFormVisible] = useState<boolean>(true);

  if (authServices.isLoggedIn()) {
    console.log("alo");
    return <Redirect to={from} />;
  }

  const onFinish = (values: any) => {
    console.log(values);
    ownerServices.register(values).then(() => {
      notification.success({
        message: 'Đăng ký tài khoản chủ trọ thành công !',
        duration: 3,
      });
      setFormVisible(false);
    });
  };

  const [form] = Form.useForm();


  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select style={{ width: 70 }}>
        <Option value="84">+84</Option>
      </Select>
    </Form.Item>
  );

  return (
    <div className={'login'}>
      <div className="login__body">
        <img src={logo} alt={'heyacare_logo'} />
        <h3> Quản lí và cho thuê phòng trọ thật dễ dàng 😍</h3>
        {
          formVisible ? (
            <Form
              {...formItemLayout}
              name="normal_login"
              className="login-form login__body__form"
              onFinish={onFinish}
              initialValues={{
                prefix: '84',
              }}
              scrollToFirstError
            >

              <Form.Item
                name="fullname"
                label="Họ tên đầy đủ"
                labelAlign={'left'}
                rules={[
                  {
                    required: true,
                    message: 'Hãy nhập tên của bạn!',
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="email"
                label="E-mail"
                labelAlign={'left'}
                rules={[
                  {
                    type: 'email',
                    message: 'Email không khả dụng!',
                  },
                  {
                    required: true,
                    message: 'Hãy nhập email của bạn!',
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="password"
                label="Mật khẩu"
                labelAlign={'left'}
                rules={[
                  {
                    required: true,
                    message: 'Hãy nhập mật khẩu!',
                  },
                ]}
                hasFeedback
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                name="confirm"
                label="Xác nhận mật khẩu"
                labelAlign={'left'}
                dependencies={['password']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Hãy nhập mật khẩu xác nhận!',
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject('Hai mật khẩu bạn nhập không giống nhau!');
                    },
                  }),
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                name="phone_number"
                label="Phone Number"
                labelAlign={'left'}
                rules={[{ required: true, message: 'Hãy nhập số điện thoại!' }]}
              >
                <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
              </Form.Item>

              <Form.Item
                name="address"
                label="Địa chỉ"
                labelAlign={'left'}
                rules={[
                  {
                    required: true,
                    message: 'Địa chỉ là bắt buộc!',
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="identity_card_number"
                label="Số CMT/CCCD"
                labelAlign={'left'}
                rules={[
                  {
                    required: true,
                    message: 'Số CMT/CCCD!',
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="agreement"
                valuePropName="checked"
                labelAlign={'left'}
                rules={[
                  {
                    validator: (_, value) =>
                      value ? Promise.resolve() : Promise.reject('Phải đồng ý với điều khoản của chúng tôi !'),
                  },
                ]}
                {...tailFormItemLayout}
              >
                <Checkbox>
                  Tôi đồng ý với <a href="">điều khoản của HeyaCare</a>
                </Checkbox>
              </Form.Item>
              <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit" className="login-form-button mt-base mb-base"
                        style={{ width: '100%' }}>
                  Đăng ký
                </Button>
                Đã có tài khoản <Link to={'/login'}>Đăng nhập ngay!</Link>
              </Form.Item>
            </Form>
          ) : (
            <>
              <Typography.Title level={5}>
                Chúng tôi đã gửi cho bạn một email xác nhận. Vui lòng kiểm tra để hoàn tất đăng ký tài khoản chủ trọ.
              </Typography.Title>
              Cảm ơn bạn đã trở thành một phần của Heyacare ♥️
            </>
          )
        }
      </div>
    </div>
  );
};

export default Register;