import React, { useContext, useEffect, useRef, useState } from 'react';
import io from 'socket.io-client';
import { Button, message, Tag } from 'antd';
import { StoreContext } from '../../contexts';
import { CameraOutlined, CloseOutlined, MessageOutlined } from '@ant-design/icons';

import './Chat.scss';
import { userServices } from '../../services';
import { chatHooks } from '../../hooks';

let socket: any;

const endpoint = 'localhost:5000';

 interface Message {
  content: string;
  type_of_message: string;
  sender_id: number;
  receiver_id: number;
}

interface AdminInfo {
  id: number;
  avatar_url: string
}

const Chat = (props: any) => {

  const [message, setMessage] = useState<string>('');

  const { currentUser } = useContext(StoreContext);

  const [chatIsVisible, setChatVisible] = useState(false);
  const [adminInfo, setAdminInfo] = useState<AdminInfo>({
    id: 0,
    avatar_url: '',
  });

  const {messages, setMessages} = chatHooks.useMessage(1)

  // useEffect(() => {
  //   userServices.getAdminInfo().then(res => {
  //     setAdminInfo(res.data);
  //   });
  // }, []);

  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    if (messagesEndRef && messagesEndRef.current) {
      // @ts-ignore
      messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  useEffect(scrollToBottom, [messages]);

  useEffect(() => {
    // @ts-ignore
    socket = io(endpoint);
    socket.emit('join', {
      sender_id: currentUser.id,
      receiver_id: 1,
    }, () => {
    });

    return () => {
      // socket.emit("disconnect", () => {})
      // socket.off();
    };
  }, [endpoint]);

  useEffect(() => {
    socket.on('message', (data: any) => {
      setMessages(messages => [...messages, data.message]);
    });
  }, []);

  const sendMessage = (event: any) => {
    event.preventDefault();
    if (message) {
      socket.emit('message', {
        message: {
          content: message,
          type_of_message: 0,
          receiver_id: 1,
          sender_id: currentUser.id,
        },
      }, () => setMessage(''));
    }
  };

  return (
    <>
      {
        !chatIsVisible ? (
          <div className={'chat-icon text-center'} onClick={() => setChatVisible(true)}>
            <MessageOutlined style={{ fontSize: 75, color: '#499cf3' }} />
            <div style={{ fontWeight: 'bold', color: '#499cf3' }}> Nhắn tin với Quản trị viên</div>
          </div>
        ) : (
          <div className={'chat-container'}>
            <div className={'chat-window'}>
              <div className={'chat-header'}>
                <div>Nhắn tin với quản trị viên ⚪</div>
                <div>
                  <CloseOutlined
                    style={{ fontWeight: 'bold', fontSize: '1rem', cursor: 'pointer' }}
                    onClick={() => setChatVisible(false)}
                  />
                </div>

              </div>
              <div className={'m-base messages'}>
                {messages.map(message => {

                  return <div
                    className={message.sender_id == currentUser.id ? 'message right appeared' : 'message left appeared'}>
                    <div className="text_wrapper">
                      <div className="text">{message.content}</div>
                    </div>

                  </div>;
                })}
                <div ref={messagesEndRef} />
              </div>

              <form className="form">
                <input
                  className="input"
                  type="text"
                  placeholder="Gõ môt tin nhắn ..."
                  value={message}
                  onChange={({ target: { value } }) => setMessage(value)}
                  onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null}
                />
                <Button icon={<CameraOutlined />} className="sendButton" onClick={e => sendMessage(e)}></Button>
              </form>
            </div>
          </div>
        )
      }
    </>
  );
};

export default Chat;