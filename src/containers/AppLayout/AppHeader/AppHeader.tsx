import React, { useContext, useEffect, useState } from 'react';
import { Layout, Menu, Dropdown, Avatar, Typography, Row, Col } from 'antd';
import {
  QuestionCircleOutlined,
  BellOutlined,
  DownOutlined,
  LogoutOutlined,
} from '@ant-design/icons';
import { localizationConstants } from 'constants/index';
import { localizationHelpers, userHelpers } from 'helpers';
import { t } from 'helpers/i18n';
import { IRegionItem } from 'interfaces';
import { StoreContext } from 'contexts';
import { notificationServices } from '../../../services';
import './AppHeader.scss';
import { Link } from 'react-router-dom';

const { Header } = Layout;
const { REGIONS } = localizationConstants;
const { getCurrentLanguage, changeLanguage } = localizationHelpers;
const { logout } = userHelpers;

interface Notification {
  message: string;
  accommodation_id: string;
  isRead: boolean
}

interface NotificationRes {
  notifications: Notification[];
  number_unread: number;
}

const AppHeader: React.FC = () => {
  const { currentUser } = useContext(StoreContext);

  const localizationMenu = (
    <Menu>
      {Object.values(REGIONS).map((el: IRegionItem) => (
        <Menu.Item key={el.key} onClick={() => changeLanguage(el.key)}>
          <Avatar src={el.flag} shape="square" />
          <span style={{ marginLeft: 10 }}>{el.name}</span>
        </Menu.Item>
      ))}
    </Menu>
  );

  const [notificationRes, setNotificationRes] = useState<NotificationRes>();
  const [notificationVisible, setNotificationVisible] = useState(false);

  useEffect(() => {
    if (currentUser.id) {
      notificationServices.getNotifications({
        owner_id: currentUser.id,
        _page: 1,
        _limit: 10,
      }).then((res: any) => setNotificationRes(res.data));
    }
  }, [currentUser.id]);

  const userMenu = (
    <Menu>
      <Menu.Item data-testid="btn-logout" onClick={logout}>
        <LogoutOutlined />
        <span>{t('Đăng xuất')}</span>
      </Menu.Item>
      <Menu.Item>
        <span> {t('Xem trang cá nhân')} </span>
      </Menu.Item>
    </Menu>
  );

  const currentRegion = REGIONS[getCurrentLanguage()];

  return (
    <Header className="app-header">
      {/* Link to system guideline (documentation) */}
      <QuestionCircleOutlined className="app-icon" />

      {/* You can implement notification feature here */}
      <span className={'notification'} onClick={() => setNotificationVisible(prevState => !prevState)}>
        <BellOutlined className="app-icon" />
        <span className="badge"> {notificationRes?.number_unread} </span>
        {
          notificationVisible && (
            <div className={'notification-container text-left'}>
              <Typography.Title level={4}>Thông báo</Typography.Title>
              {notificationRes?.notifications.map(item => (
                <a target={'_blank'} href={`http://localhost:3001/room/${item.accommodation_id}`}
                   className={item.isRead ? 'notification-item isRead' : 'notification-item'}>
                  {item.isRead ? item.message : (
                    <Row>
                      <Col span={21}>
                        {item.message}
                      </Col>
                      <Col span={3} className={'text-center'}>🔵</Col>
                    </Row>
                  )}
                </a>
              ))}
            </div>
          )
        }
      </span>

      <Dropdown overlay={localizationMenu} trigger={['click']}>
        <span className="app-user">
          <Avatar src={currentRegion && currentRegion.flag} shape="square" />
          <span className="name">{currentRegion && currentRegion.name}</span>
        </span>
      </Dropdown>

      <Dropdown overlay={userMenu} trigger={['click']}>
        <span className="app-user">
          <Avatar src={currentUser.avatar_url} />
          <span className="name">{currentUser.fullname}</span>
          <DownOutlined />
        </span>
      </Dropdown>
    </Header>
  );
};

export default AppHeader;
