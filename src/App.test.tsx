import React from 'react';
// @ts-ignore
import { render, wait, fireEvent } from '@testing-library/react';
import { authServices } from 'services';
import App from './App';

describe('user has not logged in yet', () => {
  jest.spyOn(authServices, 'isLoggedIn').mockReturnValueOnce(false);

  // Smoke testing (no assertions)
  it('renders correctly', () => {
    render(<App />);
  });
});

describe('user has already logged in', () => {
  it('renders correctly', () => {
    render(<App />);
  });

  it('clicks logout on header', async () => {
    const logoutMock = jest.spyOn(authServices, 'logout');
    const { getByTestId } = render(<App />);
    await wait(() => {
      fireEvent.click(getByTestId('btn-logout'));
    });
    expect(logoutMock).toHaveBeenCalled();
  });

  it('clicks toggle sidebar', async () => {
    const { getByTestId } = render(<App />);
    await wait(() => {
      fireEvent.click(getByTestId('menu-collapse-icon'));
    });
    expect(getByTestId('menu-expand-icon')).toBeInTheDocument();
  });
});
