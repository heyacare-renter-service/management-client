interface BathroomType {
  name: string;
  value: number;
}

interface KitchenType {
  name: string;
  value: number;
}

const BATHROOM_TYPES: BathroomType[] = [
  {
    name: 'Chung',
    value: 1,
  }, {
    name: 'Riêng',
    value: 0,
  },
];

const KITCHEN_TYPES: KitchenType[] = [
  {
    name: 'Không nấu ăn',
    value: -1,
  },
  {
    name: 'Nấu ăn riêng',
    value: 0,
  },
  {
    name: 'Nấu ăn chung',
    value: 1,
  },
];

export default {
  BATHROOM_TYPES,
  KITCHEN_TYPES,
};