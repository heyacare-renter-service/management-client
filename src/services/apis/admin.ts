import HeyaCareClient from './request';

const BASE_URL = 'admin';

const getOwnerRequest = (data: any) => {
  return HeyaCareClient.post(BASE_URL + '/owner/pending', data);
};

const confirmApprovalOwner = (ownerId: number) => {
  return HeyaCareClient.post(BASE_URL + '/owner/confirm', { ids: [ownerId] });
};

const getAccommodationRequest = (data: any) => {
  return HeyaCareClient.post(BASE_URL + '/accommodation/pending', data);
};

const confirmApprovalAccommodation = (accommodationId: string) => {
  return HeyaCareClient.post(BASE_URL + '/accommodation/confirm', {
    ids: [accommodationId],
  });
};

const declineAccommodation = (accommodationId: string) => {
  return HeyaCareClient.post(BASE_URL + '/accommodation/decline', {
    ids: [accommodationId],
  });
};

const getRatingRequest = (data: any) => {
  return HeyaCareClient.post(BASE_URL + '/rating/pending', data);
};

const confirmApprovalRating = (ratingId: number) => {
  return HeyaCareClient.post(BASE_URL + '/rating/confirm', { ids: [ratingId] });
};

const declineRating = (ratingId: number) => {
  return HeyaCareClient.post(BASE_URL + '/rating/remove', { ids: [ratingId] });
};

const getViewAnalytic = () => {
  return HeyaCareClient.post(BASE_URL + '/accommodation/view-analytic');
};

const getSearchAnalytic = () => {
  return HeyaCareClient.get(BASE_URL + '/accommodation/search-analytic');
};

const getUserAnalytic = () => {
  return HeyaCareClient.get(BASE_URL + '/user/analytic');
};

export default {
  getOwnerRequest,
  confirmApprovalOwner,
  getAccommodationRequest,
  confirmApprovalAccommodation,
  getRatingRequest,
  confirmApprovalRating,
  declineRating,
  declineAccommodation,
  getViewAnalytic,
  getUserAnalytic,
  getSearchAnalytic,
};
