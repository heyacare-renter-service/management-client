import HeyaCareClient from './request';

const BASE_URL = 'chat';

const getMessages = (receiverId: number) => {
  return HeyaCareClient.post(BASE_URL + '/messages', { receiver_id: receiverId, _page: 1, _limit: 10 });
};

export default {
  getMessages,
};