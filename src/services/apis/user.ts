import HeyaCareClient from './request';

const BASEURL = 'users';

const getAdminInfo = () => {
  return HeyaCareClient.get(BASEURL + '/get-admin-info');
};

export default {
  getAdminInfo,
};