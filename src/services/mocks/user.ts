export const currentUser = {
  id: 1,
  fullname: 'Ngô Sách Nhật',
  avatar_url:
    'https://lh3.googleusercontent.com/a-/AAuE7mDQFfGHKAJ9y6ZNwtoCyvvXfe6CY9b97C8ndmsg',
  email: 'nhatngo11a1@gmail.com',
  phone_number: '0384721376',
  address: '59 Khuc Thua Du, Cau Giay',
  role: 'admin',
  gender: 'Male',
};

const isLoggedIn = () => true;
const login = () => {
};
const logout = () => {
};
const getAccessToken = () => '1';
const getUserInfo = () => currentUser;
const getFullUserInfo = () => Promise.resolve(currentUser);
const denyAccess = () => {
};

export default {
  isLoggedIn,
  login,
  logout,
  getAccessToken,
  getUserInfo,
  getFullUserInfo,
  denyAccess,
};
