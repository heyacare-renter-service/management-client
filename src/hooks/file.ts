import { useState } from 'react';
import { fileServices } from '../services';

const useImage = () => {
  const [fileList, setFileList] = useState<any>([]);

  const getImages = () => {
    return fileList.map((item: any) => item.response || item.url)
  }

  return {
    fileList, setFileList, getImages
  }
};

export default {
  useImage,
};