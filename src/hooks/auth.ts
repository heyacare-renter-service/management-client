import { useState, useEffect, useContext } from 'react';
import userHelpers from 'helpers/user';
import { authServices } from 'services';
import { IRoute, IUserDetailsInfo } from 'interfaces';
import { StoreContext } from 'contexts';

const useUserInfo = () => {
  const [currentUser, setCurrentUser] = useState<IUserDetailsInfo>();

  const getFullUserInfo = async () => {
    const fullUserInfo = await authServices.getFullUserInfo().then(res => res.data.data);
    setCurrentUser(fullUserInfo);
  };

  useEffect(() => {
    getFullUserInfo().then(() => {
    });
  }, []);

  return { currentUser };
};

const useAuthorizationData = (items: IRoute[]) => {
  const { currentUser } = useContext(StoreContext);

  // Get navigation which match permissions to build menu
  const filteredNavigation = userHelpers.filterHasPermissions(
    items,
    currentUser.role,
  );

  // Only get routes which is link to a component
  const filteredRoutes = filteredNavigation.filter(
    item => !item.children && item.component,
  );

  return {
    filteredNavigation,
    filteredRoutes,
  };
};

export default {
  useUserInfo,
  useAuthorizationData,
};
