import { authServices } from 'services';
import { IRoute } from 'interfaces';
import { browserHistory } from './index';

const filterHasPermissions = (
  items: IRoute[],
  role: string,
) => {
  return items.filter(item => {
    const { roles } = item;
    if (!roles) {
      return true;
    }
    return roles.includes(role);
  });
};

const logout = () => {
  authServices.logout();
  browserHistory.push('/login');
};

export default {
  filterHasPermissions,
  logout,
};
