export interface IUserContextInfo {
  id: number;
  fullname: string;
  avatar_url: string;
  address: string;
  gender: string;
  email: string;
}

export interface IUserDetailsInfo extends IUserContextInfo {
  role: string;
}
