export interface ICity {
  code: string;
  name: string;
}

export interface IDistrict {
  code: string;
  name: string;
}

export interface IWard {
  code: string;
  name: string;
}