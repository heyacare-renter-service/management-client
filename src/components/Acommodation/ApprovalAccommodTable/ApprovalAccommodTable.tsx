import React, { useState } from 'react';
import { Button, Image, Modal, notification, Pagination, Popover, Table } from 'antd';
import { adminHooks } from '../../../hooks';
import { IAccommodationRequest } from '../../../hooks/admin';
import moment from 'moment';
import RoomPreview from '../RoomPreview';

const ApprovalAccommodTable = () => {
  const { accommodations, total, page, limit, reload, setPage, setReload, confirmApprovalAccommodation, declineAccommodation } = adminHooks.useAccommodationRequest();
  const [isPreviewVisible, setIsPreviewVisible] = useState(false);

  const [editId, setEditId] = useState('');

  const data = accommodations.map(item => {
    return {
      room: item,
      setReload: (isRefresh) => setReload(isRefresh),
      approve: (accommodationId) => confirmApprovalAccommodation(accommodationId),
      setIsPreviewVisible: setIsPreviewVisible,
      setEditId: setEditId,
      decline: accommodationId => declineAccommodation(accommodationId),
    } as IRecord;
  });


  return (
    <div>
      <Table
        // @ts-ignore
        columns={columns}
        dataSource={data}
        bordered
        pagination={false}
        loading={reload}
      />
      <Pagination
        className={'mt-base text-center'}
        total={total}
        current={page}
        onChange={(page, pageSize) => setPage(page)}
      />

      <Modal
        title={'Xem thông tin bài đăng'}
        visible={isPreviewVisible}
        onCancel={() => {
          setIsPreviewVisible(false);
          setEditId('');
        }}
        footer={null}
        width={600}
      >
        <RoomPreview roomId={editId} />
      </Modal>
    </div>
  );
};

interface IRecord {
  room: IAccommodationRequest;
  approve: (accommodationId: string) => Promise<any>;
  setReload: (isRefresh: boolean) => void;
  setIsPreviewVisible: (visible: boolean) => void;
  setEditId: any;
  decline: (accommodationId: string) => Promise<any>;
}

const columns = [
  {
    title: 'Ảnh thu nhỏ',
    key: 'image',
    render: (record: IRecord) => <div>
      <Popover content={'Xem'}>
        <Image style={{ width: '50px', cursor: 'pointer' }} src={record.room.images[0]} />
      </Popover>
    </div>,
    align: 'center',
  },
  {
    title: 'Tiêu đề',
    key: 'title',
    render: (record: IRecord) => (<> {record.room.title} </>),
  },
  {
    title: 'Chủ trọ',
    key: 'owner',
    render: (record: IRecord) => (<> {record.room.owner.fullname} </>),
  },
  {
    title: 'Ngày yêu cầu',
    key: 'created_at',
    render: (record: IRecord) => (<> {moment(record.room.created_at).format('HH:mm DD-MM-YYYY')} </>),
  },
  {
    title: 'Xem chi tiết',
    key: 'view',
    render: (record: IRecord) => (
      <Button
        type={'link'}
        onClick={() => {
          record.setEditId(record.room.id);
          record.setIsPreviewVisible(true);
        }}
      >Xem đầy đủ thông tin</Button>
    ),
    align: 'center',
  },
  {
    title: 'Phê duyệt',
    key: 'approve',
    render: (record: IRecord) => (
      <>
        <Button
          type={'link'}
          onClick={() => {
            record.approve(record.room.id).then(() => {
              notification.success({
                message: 'Phê duyệt bài viết thành công !',
                duration: 3,
              });
              record.setReload(true);
            });
          }}
        > Phê duyệt
        </Button>
        <Button
          type={'link'}
          onClick={() => {
            record.decline(record.room.id).then(() => {
              notification.success({
                message: 'Từ chối bài viết thành công !',
                duration: 3,
              });
              record.setReload(true);
            });
          }}
        > Từ chối
        </Button>
      </>
    ),
    align: 'center',
  },
];

export default ApprovalAccommodTable;