import React, { useEffect, useState } from 'react';
import { Comment, Tooltip, Avatar, Rate, Empty } from 'antd';
import moment from 'moment';
import { StarFilled } from '@ant-design/icons';
import { accommodationServices } from '../../../../services';

export const RatingAndCommentItem = (props: ItemProps) => {
  const { item } = props;
  return (
    <div className={'m-base'}>
      <Comment
        author={<a>{item.user_name}</a>}
        avatar={
          <Avatar
            src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
            alt={item.user_name}
          />
        }
        content={
          <p>
            {item.comment}
          </p>
        }
        datetime={
          <Tooltip title={moment(item.created_at).utc().local().format('YYYY-MM-DD HH:mm:ss')}>
            <span>{moment(item.created_at).utc().local().fromNow()}</span>
          </Tooltip>
        }
      />
      <p style={{ fontSize: '0.8rem', color: 'gray' }}>
        Đánh giá :
        <Rate style={{ fontSize: '0.8rem' }} defaultValue={item.star} disabled />
      </p>
    </div>
  );
};

const RatingAndComment = (props: IProps) => {
  const [data, setData] = useState<IResponse>({
    ratings: [],
    average_rating: 0,
  });

  useEffect(() => {
    if (props.accommodationId.length > 0) {
      accommodationServices.getRatingAndComments(props.accommodationId).then(
        res => setData(res.data.data),
      );
    }
  }, [props.accommodationId]);

  return (
    <>
      <h2 className={'text-center'}>Đánh giá của người dùng</h2>
      <h4>Trung bình : {data.average_rating} <StarFilled style={{ color: 'yellow', fontSize: 20 }} /></h4>
      {data.ratings.map(item => <RatingAndCommentItem item={item} />)}
      {data.ratings.length === 0 &&
      <Empty description={'Chưa có đánh giá nào ở đây cả, vui lòng quay lại xem sau <3'} />}
    </>
  );
};

interface ItemProps {
  item: IRatingAndComment
}

interface IProps {
  accommodationId: string
}

export interface IRatingAndComment {
  id: number;
  user_name: string;
  star: number;
  comment: string;
  created_at: string;
}

interface IResponse {
  ratings: IRatingAndComment[];
  average_rating: number;
}

export default RatingAndComment;