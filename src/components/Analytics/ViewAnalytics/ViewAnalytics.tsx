import React, { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import { Col, Row } from 'antd';
import { adminServices } from '../../../services';

const ViewAnalytics = () => {
  const [views, setViews] = useState<IView[]>([]);
  const [searchs, setSearchs] = useState<IView[]>([]);

  const viewLabels = views.map(item => item.time);
  const viewData = views.map(item => item.value);

  const searchLables = searchs.map(item => item.time);
  const searchData = searchs.map(item => item.value);

  useEffect(() => {
    adminServices.getViewAnalytic().then(res => {
      setViews(res.data.data);
    });

    adminServices.getSearchAnalytic().then(res => {
      setSearchs(res.data.data);
    });
  }, []);

  return (
    <Row className={'text-center'}>
      <Col span={12}>
        <Line
          data={{
            labels: viewLabels,
            datasets: [
              {
                data: viewData,
                label: 'Xem trang',
                borderColor: '#3e95cd',
                fill: false,
              },
            ],
          }}
          options={{
            title: {
              display: true,
              text: 'Lưu lượng truy cập',
            },
            legend: {
              display: true,
              position: 'bottom',
            },
          }}
        />
      </Col>
      <Col span={12}>
        <Line
          data={{
            labels: searchLables,
            datasets: [
              {
                data: searchData,
                label: 'Tìm kiếm',
                borderColor: '#3e95cd',
                fill: false,
              },
            ],
          }}
          options={{
            title: {
              display: true,
              text: 'Lưu lượng tìm kiếm',
            },
            legend: {
              display: true,
              position: 'bottom',
            },
          }}
        />
      </Col>
    </Row>
  );
};

interface IView {
  time: string;
  value: number;
}

export default ViewAnalytics;
