import React, { useEffect, useState } from 'react';
import { Col, Row, Statistic } from 'antd';
import { adminServices } from '../../../services';

interface Analytic {
  user_count: number;
  owner_count: number;
  view_count: number;
  accommodation_count: number;
  rating_avg: number;
}

const UserAnalytics = () => {
  const [data, setData] = useState<Analytic>({
    user_count: 0,
    owner_count: 0,
    view_count: 0,
    accommodation_count: 0,
    rating_avg: 0,
  });

  useEffect(() => {
    adminServices.getUserAnalytic().then(res => {
      setData(res.data.data);
    });
  }, []);

  return (
    <Row gutter={16} justify={'space-around'}>
      <Col span={8}>
        <Row>
          <Col span={12}>
            <Statistic title="Người dùng hoạt động" value={data.user_count} />
          </Col>
          <Col span={12}>
            <Statistic title="Chủ trọ" value={data.owner_count} />
          </Col>
        </Row>
      </Col>
      <Col span={8}>
        <Row>
          <Col span={8}>
            <Statistic title="Số bài đăng" value={data.accommodation_count} />
          </Col>
          <Col span={8}>
            <Statistic title="Lượt xem" value={data.view_count} />
          </Col>
          <Col span={8}>
            <Statistic
              title="Lượt đánh giá trung bình"
              value={data.rating_avg}
              precision={2}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default UserAnalytics;
