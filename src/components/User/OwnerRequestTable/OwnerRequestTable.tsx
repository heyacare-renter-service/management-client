import React, { useState } from 'react';
import { Button, Image, notification, Pagination, Popover, Table } from 'antd';
import moment from 'moment';
import { IOwnerRequest } from '../../../hooks/admin';
import { adminHooks } from '../../../hooks';
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';

const OwnerRequestTable = () => {
  const { owners, totalOwners, page, limit, confirmApprovalOwner, setReload, reload, setPage } = adminHooks.useOwnerRequest();
  const data: IRecord[] = owners.map(item => (
    { owner: item, approve: confirmApprovalOwner, setReload: (isRefresh => setReload(isRefresh)) } as IRecord
  ));


  return (
    <>
      <Table
        // @ts-ignore
        columns={columns}
        dataSource={data}
        bordered
        pagination={false}
        loading={reload}
      />
      <Pagination
        className={'mt-base text-center'}
        total={totalOwners}
        current={page}
        onChange={(page, pageSize) => setPage(page)}
      />
    </>
  );
};

interface IRecord {
  owner: IOwnerRequest;
  approve: (ownerId: number) => Promise<any>;
  setReload: (isRefresh: boolean) => void;
}

const columns = [
  {
    title: 'Ảnh đại diện',
    key: 'avatar',
    render: (record: IRecord) => <div>
      <Popover content={'Xem'}>
        <Image style={{ width: '50px', cursor: 'pointer' }} src={record.owner.avatar_url} />
      </Popover>
    </div>,
    align: 'center',
  },
  {
    title: 'Họ và tên',
    key: 'fullname',
    render: (record: IRecord) => <div> {record.owner.fullname} </div>,
    align: 'center',
  },
  {
    title: 'Thư điện tử',
    key: 'email',
    render: (record: IRecord) => <div> {record.owner.email} </div>,
    align: 'center',
  },
  {
    title: 'Số điện thoại',
    key: 'phone_number',
    render: (record: IRecord) => <div> {record.owner.phone_number} </div>,
    align: 'center',
  },
  {
    title: 'Số CMT/CCCD',
    key: 'phone_number',
    render: (record: IRecord) => <div> {record.owner.identity_card_number} </div>,
    align: 'center',
  },
  {
    title: 'Địa chỉ',
    key: 'address',
    render: (record: IRecord) => <div> {record.owner.address} </div>,
  },
  {
    title: 'Thời gian yêu cầu',
    key: 'created_at',
    render: (record: IRecord) => <div> {moment(record.owner.created_at).format('DD-MM-YYYY HH:mm:ss')} </div>,
    align: 'center',
  },
  {
    title: 'Xử lý',
    key: 'action',
    render: (record: IRecord) => <div>
      <Button
        icon={<CheckOutlined />}
        type={'link'}
        onClick={() => {
          record.approve(record.owner.id).then(() => {
            notification.success({
              message: 'Phê duyệt thành công !',
              duration: 2,
            });
            record.setReload(true);
          });
        }
        }
      >
        Phê duyệt</Button>
      <Button icon={<CloseOutlined />} type={'link'}> Từ chối </Button>
    </div>,
    align: 'center',
  },
];

export default OwnerRequestTable;