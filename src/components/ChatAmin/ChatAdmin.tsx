import React, { useEffect, useRef, useState } from 'react';
import io from 'socket.io-client';
import { CameraOutlined, CloseOutlined, MessageOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { chatHooks } from '../../hooks';

let socket: any;

const endpoint = 'localhost:5000';

interface Message {
  content: string;
  type_of_message: string;
  sender_id: number;
  receiver_id: number;
}

const ChatAdmin = (props: Iprops) => {
  const { messages, setMessages } = chatHooks.useMessage(props.receiverId);
  const [message, setMessage] = useState<string>('');

  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    if (messagesEndRef && messagesEndRef.current) {
      // @ts-ignore
      messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  useEffect(scrollToBottom, [messages]);

  useEffect(() => {
    // @ts-ignore
    socket = io(endpoint);
    socket.emit('join', {
      sender_id: props.senderId,
      receiver_id: props.receiverId,
    }, () => {
    });

    return () => {
      // socket.emit("disconnect", () => {})
      // socket.off();
    };
  }, [endpoint]);

  useEffect(() => {
    socket.on('message', (data: any) => {
      console.log(data);
      setMessages(messages => [...messages, data.message]);
    });
  }, []);

  const sendMessage = (event: any) => {
    event.preventDefault();
    if (message) {
      socket.emit('message', {
        message: {
          content: message,
          type_of_message: 0,
          receiver_id: props.receiverId,
          sender_id: props.senderId,
        },
      }, () => setMessage(''));
    }
  };

  return (
    <>
      {
        <div className={'chat-container'}>
          <div className={'chat-window'}>
            <div className={'chat-header'}>
              <div>Nhắn tin với quản trị viên ⚪</div>
            </div>
            <div className={'m-base messages'}>
              {messages.map(message => {

                return <div
                  className={message.sender_id == props.senderId ? 'message right appeared' : 'message left appeared'}>
                  <div className="text_wrapper">
                    <div className="text">{message.content}</div>
                  </div>

                </div>;
              })}
              <div ref={messagesEndRef} />
            </div>

            <form className="form">
              <input
                className="input"
                type="text"
                placeholder="Gõ môt tin nhắn ..."
                value={message}
                onChange={({ target: { value } }) => setMessage(value)}
                onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null}
              />
              <Button icon={<CameraOutlined />} className="sendButton" onClick={e => sendMessage(e)}></Button>
            </form>
          </div>
        </div>

      }
    </>
  );
};

interface Iprops {
  senderId: number;
  receiverId: number;
  receiverName?: string;
}

export default ChatAdmin;